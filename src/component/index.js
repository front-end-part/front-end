import NavBar from "./NavBar";
import LandingPage from "./landingpage";
import CarouselBanner from "./Carousel";
import HalamanProduk from "./HalamanProduk";
import InfoProduk from "./InfoProduk";
import InfoProfil from "./InfoProfil";
import InfoPenawaran from "./InfoPenawaran";
import Footer from "./Footer";
import DaftarJual from "./DaftarJual";
import DaftarDiminati from "./DaftarDiminati";

export {
  NavBar,
  LandingPage,
  CarouselBanner,
  HalamanProduk,
  InfoProfil,
  InfoProduk,
  InfoPenawaran,
  Footer,
  DaftarJual,
  DaftarDiminati,
};