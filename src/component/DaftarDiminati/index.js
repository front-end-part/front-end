import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import NavBar from "../NavBar";
import "../../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  FiBox,
  FiHeart,
  FiDollarSign,
  FiChevronRight,
  FiPlus,
} from "react-icons/fi";

export default function DaftarDiminati() {

  return (
    <div className="container">
      <div>
        <NavBar />
      </div>
      <div>
        <p className="fw-bold my-3">Daftar Jual Saya</p>
        <div className="row">
            <>
              <div
                className="col-xl-1 col-sm-1 col-1 d-flex"
                style={{ width: "70px" }}
              >
                <img 
                  alt=""
                  className="img-fluid align-items-center"
                  style={{ width: "70px", objectFit: "contain" }}
                />
              </div>
              <div className="col-xl-10  col-sm-6 col-9">
                <p className="fw-bold mb-1" style={{ fontSize: "16px" }}>
                </p>
                <p className="mb-1" style={{ fontSize: "14px" }}>
                </p>
              </div>
            </>
          <div className="col-xl-1  col-sm-3 col-1">
            <button className="btn btn-custom borderradius8">Edit</button>
          </div>
        </div>
      </div>
      <div>
        <div className="row my-4 ">
          <div className="col-3 card p-3">
            <p className="fw-bold" style={{ fontSize: "16px" }}>
              Kategori
            </p>
            <table>
              <tr>
                <td>
                  <FiBox />
                </td>
                <td>Semua Produk</td>
                <td>
                  <FiChevronRight />
                </td>
              </tr>

              <tr>
                <td>
                  <FiHeart />
                </td>
                <td>
                  <a href={`/daftarDiminati`}>Diminati</a>
                </td>
                <td>
                  <FiChevronRight />
                </td>
              </tr>
              <tr>
                <td>
                  <FiDollarSign />
                </td>
                <td>Terjual</td>
                <td>
                  <FiChevronRight />
                </td>
              </tr>
            </table>
          </div>
          <div className="col-9 ">
            <div className="row justify-content-center">
              <div
                className="col-xl-3 col-md-5 col-sm-12 card m-2"
                style={{
                  border: "2px dashed",
                  color: "#D0D0D0",
                  height: "200px",
                  boxShadow: "0px 0px 4px rgba(0, 0, 0, 0.15)",
                  borderRadius: "4px",
                }}
              >
                <Link
                  to="/infoproduk"
                  className="text-decoration-none m-auto align-items-center"
                  style={{ color: "black" }}
                >
                  <div className="ms-5">
                    <FiPlus />
                  </div>
                  <div>
                    <p>Tambah Produk</p>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}